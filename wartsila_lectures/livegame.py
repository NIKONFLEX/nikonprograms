import pygame
import random

WIDTH = 650
HEIGHT = 650
FPS = 5

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
FIELD_SIDE = 100
OFFSET = 25
CELL_SIZE = (HEIGHT - OFFSET * 2 + FIELD_SIDE) / FIELD_SIDE
STEP = CELL_SIZE - 1

pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()
new_era = [(10, 10), (11, 11), (12, 11), (12, 10), (12, 9)]

def draw_field():
    for i in range(FIELD_SIDE):
        for j in range(FIELD_SIDE):
            pygame.draw.rect(screen, (210,210,210), (OFFSET + STEP * j, OFFSET + STEP * i, CELL_SIZE, CELL_SIZE), 1)

def pixel_to_cell(pos):
    x_cell = 0
    y_cell = 0
    x_cell = (pos[0] - OFFSET) // STEP
    y_cell = (pos[1] - OFFSET) // STEP
    return (int(x_cell), int(y_cell))

def change_color(tuples_list):
    for cell in tuples_list:
        change_cell = (OFFSET + STEP * cell[0], OFFSET + STEP * cell[1])
        pygame.draw.rect(screen, (10, 10, 10), (change_cell[0], change_cell[1], CELL_SIZE, CELL_SIZE))

def num_live_neigbours(lives_cells_list, x, y, distance):
    live_neigh = 0
    for i in range(-distance, distance+1):
        for j in range(-distance, distance+1):
            check_x = (x + i) % FIELD_SIDE 
            check_y = (y + j) % FIELD_SIDE
            if i == 0 and j == 0:
                continue
            if (check_x, check_y) in lives_cells_list:
                live_neigh += 1
    return live_neigh

all_cellc_colors = [ [WHITE]*FIELD_SIDE for i in range(FIELD_SIDE) ]

def evolution(lives_cells_list):
    height = FIELD_SIDE    
    width = FIELD_SIDE   
    return_list = []  
    for y in range(height):
        for x in range(width):
            live = (x, y) in lives_cells_list
            live_neigh = num_live_neigbours(lives_cells_list, x, y,1)

            if live_neigh == 3 and not live:
                live = True
            if live_neigh > 3 or live_neigh < 2:
                live = False

            if live:
                return_list.append((x, y))
            
        
    return return_list
    
game_mode = False

# Цикл игры
running = True
while running:
    # Держим цикл на правильной скорости
    clock.tick(FPS if game_mode else FPS*100)
    # Ввод процесса (события)
    for event in pygame.event.get():
        # check for closing window
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEMOTION and event.buttons[0] == 1:
            mouse_pos = pixel_to_cell(event.pos)
            new_era.append(mouse_pos)

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                game_mode = True
    # Обновление
    
    # Рендеринг
    screen.fill(WHITE)
    draw_field()
    if game_mode:
        new_era = evolution(new_era)
    change_color(new_era)
    # После отрисовки всего, переворачиваем экран
    pygame.display.flip()

pygame.quit()