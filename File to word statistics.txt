
SECTIONS
SEARCH
SKIP TO CONTENTSKIP TO SITE INDEXTECHNOLOGY
SUBSCRIBE NOW
LOG IN
TECHNOLOGY
|
Virus Conspiracists Elevate a New Champion

The Coronavirus Outbreak 
 Latest Updates
Maps and Tracker
Impact on Workers
Life at Home
Newsletter
ADVERTISEMENT

Continue reading the main story
Virus Conspiracists Elevate a New Champion
A video showcasing baseless arguments by Dr. Judy Mikovits, including attacks on Dr. Anthony Fauci, has been viewed more than eight million times in the past week.


Dr. Judy Mikovits at the Whittemore Peterson Institute for Neuro-Immune Disease in 2011, the year she was fired there.
Dr. Judy Mikovits at the Whittemore Peterson Institute for Neuro-Immune Disease in 2011, the year she was fired there.Credit...David Calvert/Associated Press
By Davey Alba
May 9, 2020

In a video posted to YouTube on Monday, a woman animatedly described an unsubstantiated secret plot by global elites like Bill Gates and Dr. Anthony Fauci to use the coronavirus pandemic to profit and grab political power.

In the 26-minute video, the woman asserted how Dr. Fauci, the director of the National Institute of Allergy and Infectious Diseases and a leading voice on the coronavirus, had buried her research about how vaccines can damage people’s immune systems. It is those weakened immune systems, she declared, that have made people susceptible to illnesses like Covid-19.

The video, a scene from a longer dubious documentary called “Plandemic,” was quickly seized upon by anti-vaccinators, the conspiracy group QAnon and activists from the Reopen America movement, generating more than eight million views. And it has turned the woman — Dr. Judy Mikovits, 62, a discredited scientist — into a new star of virus disinformation.

Her ascent was powered not only by the YouTube video but also by a book that she published in April, “Plague of Corruption,” which frames Dr. Mikovits as a truth-teller fighting deception in science. In recent weeks, she has become a darling of far-right publications like The Epoch Times and The Gateway Pundit. Mentions of her on social media and television have spiked to as high as 14,000 a day, according to the media insights company Zignal Labs.

ADVERTISEMENT

Continue reading the main story

The rise of Dr. Mikovits is the latest twist in the virus disinformation wars, which have swelled throughout the pandemic. Conspiracy theorists have used the uncertainty and fear around the disease to mint many villains. Those include Dr. Fauci after he appeared to slight President Trump and Mr. Gates, a co-founder of Microsoft, as someone who started the disease. They have also pushed the baseless idea that 5G wireless waves can help cause the disease.

On the flip side, they have created their own heroes, like Dr. Mikovits.

The conspiracy theorists “recast a pusher of discredited pseudoscience as a whistle-blowing counterpoint to real expertise,” said Renee DiResta, a disinformation researcher at the Stanford Internet Observatory.

Unlock more free articles.
Create an account or log in
Dr. Mikovits did not respond to requests for comment.

Judy Mikovits has a degree in biology from the University of Virginia and a Ph.D. in molecular biology from George Washington University. From 1992 to 2001, she worked at the National Cancer Institute as a postdoctoral fellow, a staff scientist and a lab director, then served as research director of the Whittemore Peterson Institute for Neuro-Immune Disease from 2006 to 2011. In 2011, after her research into chronic fatigue syndrome was discredited, she was fired from Whittemore.

Dr. Mikovits’s rise to internet notoriety has been sudden. According to data from Zignal Labs, she was rarely mentioned on social media platforms in February.


ImageGoFundMe removed a fund-raiser for Judy Mikovits, stating that the campaign violated the website’s terms of service.
GoFundMe removed a fund-raiser for Judy Mikovits, stating that the campaign violated the website’s terms of service.
By April, coverage of Dr. Mikovits rose to 800 mentions a day. That month, Darla Shine, the wife of Bill Shine, a former Fox News executive and former top aide to Mr. Trump, promoted Dr. Mikovits’s book in a tweet. Videos by The Epoch Times, a publication with ties to the Falun Gong, and the conservative outlet “The Next News Network” interviewed Dr. Mikovits about the pandemic, generating more than 1.5 million views on social networks.

ADVERTISEMENT

Continue reading the main story
Then came the video from “Plandemic,” which made mentions of Dr. Mikovits on social media spike far higher. The video was produced by Mikki Willis, who was involved in making “Bernie or Bust” and “Never Hillary” videos during the 2016 presidential campaign.

Latest Updates: Economy
Tesla’s plans to reopen its California factory are not in compliance with a local health order.
April’s job losses highlight the depth of the pandemic’s devastation.
Women are embracing a natural appearance in lockdown.
See more updates
Updated 33h ago
More live coverage: Global U.S. New York
Her arguments also began to spill over into the real world, including her baseless assertion that “wearing the mask literally activates your own virus.” There is no evidence that wearing a mask can activate viruses and make people sick. On Thursday in Sacramento, Calif., a woman brandished a sign in front of the state Capitol building that read, “Do you know who Dr. Judy Mikovits is? Then don’t tell me I need a silly mask.”

YouTube and Facebook have removed the “Plandemic” scene, saying that it spread inaccurate information about Covid-19 that could be harmful to the public. But the video continues to circulate, as people post new copies. Twitter added an “unsafe” warning on at least one link featuring Dr. Mikovits on the social network, and blocked the hashtags #PlagueOfCorruption and #Plandemicmovie from trends and search.

Dr. Mikovits has attacked Dr. Fauci online since at least 2018. But her claims did not gain much traction until this year, when the narrative that Dr. Fauci was secretly plotting to undermine and discredit the president started spreading.

Sign up to receive an email when we publish a new story about the coronavirus outbreak.
Sign Up
Dr. Mikovits says Dr. Fauci’s attacks on her work date back to the 1980s, when she contributed research to the National Cancer Institute as a graduate student. In the video being shared, Dr. Mikovits alleges that Dr. Fauci intercepted her research on H.I.V. to make money off patents, threatened her and then took undeserved credit for moving the field of H.I.V. treatment forward.

She also ties her professional downfall to Dr. Fauci. In 2009, Dr. Mikovits published research in the journal Science claiming to show that a mouse retrovirus caused chronic fatigue syndrome and other illnesses. That research gained significant media attention, but it was discredited a couple of years later, including with a retraction by the journal. Dr. Mikovits was briefly jailed in California on charges of theft made by Whittemore. The charges were later dropped.

ADVERTISEMENT

Continue reading the main story
Dr. Mikovits has sought to reframe the scandal as part of a broader campaign of persecution, aimed at silencing her work questioning the safety of vaccines.

There is no evidence that Dr. Fauci and Dr. Mikovits interacted. This week, in a statement to the fact-checking website Snopes, Dr. Fauci denied ever having threatened Dr. Mikovits. “I have no idea what she is talking about,” he wrote.

The National Cancer Institute referred an inquiry about Dr. Mikovits’s claims to the National Institutes of Health, the agency that oversees the N.C.I.’s cancer research and training. Dr. Fauci came to the National Institutes of Health as a clinical associate in 1968, and was appointed director of the National Institute of Allergy and Infectious Diseases at the N.I.H. by 1984.

In a statement, the agency said, “The National Institutes of Health and National Institute of Allergy and Infectious Diseases are focused on critical research aimed at ending the Covid-19 pandemic and preventing further deaths. We are not engaging in tactics by some seeking to derail our efforts.”

Dr. Ian Lipkin, the director of the Center for Infection and Immunity at Columbia University, said in an interview on Saturday morning that Dr. Fauci had asked him in 2011 to design a study that would address whether Dr. Mikovits and others could reproduce her research showing an association between XMRV, the mouse retrovirus, and chronic fatigue syndrome. He pointed to a September 2012 news conference at Columbia in which Dr. Mikovits admitted the link her original research had made between the mouse retrovirus and chronic fatigue syndrome was “simply not there.”

“Now is the time to use” the invalidating results that came out of the effort to reproduce her research “and move forward,” Dr. Mikovits said at the time. “And that’s what science is all about.”

Ivan Oransky, a co-founder of the academic watchdog Retraction Watch, which has followed Dr. Mikovits’s work closely, said that when he sees videos like the one posted in the past week, “they tend to coalesce around certain kinds of subjects, then the trajectory turns to martyrhood really quickly.”

ADVERTISEMENT

Continue reading the main story
There is some evidence that prominent members of conspiracy groups have tried to give her name and her story a lift online.

Zach Vorhies, a former YouTube employee who has recently promoted QAnon conspiracy theories, posted a GoFundMe campaign on April 19 titled “Help me amplify Pharma Whistleblower Judy Mikovits.” The campaign was first spotted by Ms. DiResta, of the Stanford Internet Observatory.

A day before the GoFundMe campaign began, a newly created account for Dr. Mikovits tweeted for the first time. “A big thanks goes out to Zach Vorhies (@Perpetualmaniac) for helping me get on Twitter!” It was retweeted 400 times and liked more than 2,200 times. The account has gained over 111,000 followers in less than a month.

GoFundMe removed the page on Friday, stating that the campaign violated the website’s terms of service for “campaigns that are fraudulent, misleading, inaccurate, dishonest, or impossible.”

Mr. Vorhies did not respond to requests for comment.

Dr. Mikovits’s newfound notoriety has also lifted sales of her new book. This week, “Plague of Corruption” shot to No. 1 on Amazon’s print best-seller list. The book was out of stock on Friday. Amazon said that the book did not violate the company’s content guidelines.

Skyhorse, the independent publishing company behind the book, defended its decision to print Dr. Mikovits. “The world should discuss the ideas in this book, rather than allow censorship to prevail,” a spokeswoman for Skyhorse said.

Dr. Peter J. Hotez, dean of the National School of Tropical Medicine at Baylor College of Medicine, said her rise illustrated how the anti-vaccination movement had “taken a new ominous twist” with the coronavirus.

ADVERTISEMENT

Continue reading the main story
“They’ve now aligned themselves with far-right groups,” Dr. Hotez said, “and their weapons of choice are YouTube, Facebook and Amazon.”

Sheera Frenkel and Alexandra Alter contributed reporting. Ben Decker and Jack Begg contributed research.

The Coronavirus Outbreak
Frequently Asked Questions and Advice
Updated April 11, 2020

What should I do if I feel sick?
If you’ve been exposed to the coronavirus or think you have, and have a fever or symptoms like a cough or difficulty breathing, call a doctor. They should give you advice on whether you should be tested, how to get tested, and how to seek medical treatment without potentially infecting or exposing others.

When will this end?
This is a difficult question, because a lot depends on how well the virus is contained. A better question might be: “How will we know when to reopen the country?” In an American Enterprise Institute report, Scott Gottlieb, Caitlin Rivers, Mark B. McClellan, Lauren Silvis and Crystal Watson staked out four goal posts for recovery: Hospitals in the state must be able to safely treat all patients requiring hospitalization, without resorting to crisis standards of care; the state needs to be able to at least test everyone who has symptoms; the state is able to conduct monitoring of confirmed cases and contacts; and there must be a sustained reduction in cases for at least 14 days.

How can I help?
The Times Neediest Cases Fund has started a special campaign to help those who have been affected, which accepts donations here. Charity Navigator, which evaluates charities using a numbers-based system, has a running list of nonprofits working in communities affected by the outbreak. You can give blood through the American Red Cross, and World Central Kitchen has stepped in to distribute meals in major cities. More than 30,000 coronavirus-related GoFundMe fund-raisers have started in the past few weeks. (The sheer number of fund-raisers means more of them are likely to fail to meet their goal, though.)

Should I wear a mask?
The C.D.C. has recommended that all Americans wear cloth masks if they go out in public. This is a shift in federal guidance reflecting new concerns that the coronavirus is being spread by infected people who have no symptoms. Until now, the C.D.C., like the W.H.O., has advised that ordinary people don’t need to wear masks unless they are sick and coughing. Part of the reason was to preserve medical-grade masks for health care workers who desperately need them at a time when they are in continuously short supply. Masks don’t replace hand washing and social distancing.

How do I get tested?
If you’re sick and you think you’ve been exposed to the new coronavirus, the C.D.C. recommends that you call your healthcare provider and explain your symptoms and fears. They will decide if you need to be tested. Keep in mind that there’s a chance — because of a lack of testing kits or because you’re asymptomatic, for instance — you won’t be able to get tested.

How does coronavirus spread?
It seems to spread very easily from person to person, especially in homes, hospitals and other confined spaces. The pathogen can be carried on tiny respiratory droplets that fall as they are coughed or sneezed out. It may also be transmitted when we touch a contaminated surface and then touch our face.

Is there a vaccine yet?
No. Clinical trials are underway in the United States, China and Europe. But American officials and pharmaceutical executives have said that a vaccine remains at least 12 to 18 months away.

What makes this outbreak so different?
Unlike the flu, there is no known treatment or vaccine, and little is known about this particular virus so far. It seems to be more lethal than the flu, but the numbers are still uncertain. And it hits the elderly and those with underlying conditions — not just those with respiratory diseases — particularly hard.

What if somebody in my family gets sick?
If the family member doesn’t need hospitalization and can be cared for at home, you should help him or her with basic needs and monitor the symptoms, while also keeping as much distance as possible, according to guidelines issued by the C.D.C. If there’s space, the sick family member should stay in a separate room and use a separate bathroom. If masks are available, both the sick person and the caregiver should wear them when the caregiver enters the room. Make sure not to share any dishes or other household items and to regularly clean surfaces like counters, doorknobs, toilets and tables. Don’t forget to wash your hands frequently.

Should I stock up on groceries?
Plan two weeks of meals if possible. But people should not hoard food or supplies. Despite the empty shelves, the supply chain remains strong. And remember to wipe the handle of the grocery cart with a disinfecting wipe and wash your hands as soon as you get home.

Can I go to the park?
Yes, but make sure you keep six feet of distance between you and people who don’t live in your home. Even if you just hang out in a park, rather than go for a jog or a walk, getting some fresh air, and hopefully sunshine, is a good idea.

Should I pull my money from the markets?
That’s not a good idea. Even if you’re retired, having a balanced portfolio of stocks and bonds so that your money keeps up with inflation, or even grows, makes sense. But retirees may want to think about having enough cash set aside for a year’s worth of living expenses and big payments needed over the next five years.

What should I do with my 401(k)?
Watching your balance go up and down can be scary. You may be wondering if you should decrease your contributions — don’t! If your employer matches any part of your contributions, make sure you’re at least saving as much as you can to get that “free money.”

ADVERTISEMENT

Continue reading the main story
Up-to-date information on coronavirus is available for free.
Read more for free.
COLLAPSE



Up-to-date information on coronavirus is available for free.

CONTINUE
Site Index
Site Information Navigation
© 2020 The New York Times Company
NYTCoContact UsWork with usAdvertiseT Brand StudioYour Ad ChoicesPrivacyTerms of ServiceTerms of SaleSite MapHelpSubscriptions
