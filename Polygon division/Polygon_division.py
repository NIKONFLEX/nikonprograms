import pygame
import random
import Actions_with_polygon
from Polygon_class import Polygon
from Point_class import Point
from Line_class import Line
import Intercection_funct


WIDTH = 800
HEIGHT = 650
FPS = 30

# Задаем цвета
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

# Создаем игру и окно
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()

def draw_circle_in_vertex(vertex:Point):
    pygame.draw.circle(screen, BLACK, (vertex.x, vertex.y), 3)

def download_picture():
    static_polygon_and_line = Actions_with_polygon.read_file_with_polygon_and_line()
    polygon = Polygon(Point(static_polygon_and_line.polygon[0][0], static_polygon_and_line.polygon[0][1]))
    for pair in static_polygon_and_line.polygon:
        vertex = Point(pair[0], pair[1])
        polygon.append_vertex(vertex)

    line = Line(Point(static_polygon_and_line.line[0], static_polygon_and_line.line[1]))
    line.record_finish_point(Point(static_polygon_and_line.line[2], static_polygon_and_line.line[3]))

    return [polygon], [line]

def save_picture(polygons_list, lines_list):
    polygon_list_to_save = polygons_list[-1].points_list_to_pairs_list()
    line_param = lines_list[-1].get_points()
    lines_list_to_save = [line_param[0].x, line_param[0].y, line_param[1].x, line_param[1].y]
    Actions_with_polygon.Saved_image(polygon_list_to_save, lines_list_to_save).write_polygon_and_line()

def splitting_a_polygon_into_edges(polygon):
    edges_list = []

    for i in range(len(polygon.vertex_list)):
        edge = Line(polygon.vertex_list[i])
        if i < len(polygon.vertex_list) - 1:
            edge.record_finish_point(polygon.vertex_list[i + 1])
        else:
            edge.record_finish_point(polygon.vertex_list[0])

        edges_list.append(edge)

    return edges_list

static_polygon_and_line = Actions_with_polygon.read_file_with_polygon_and_line()

polygons_list = []
lines_list = []
work_with_polygon = False
work_with_line = False
finish_polygon = False
finish_line = False
intercection_points_list = []

# Цикл игры
running = True
while running:
    if len(polygons_list) != 0 and len(lines_list) != 0:
        edges_list = splitting_a_polygon_into_edges(polygons_list[0])
        for edge in edges_list:
            for line in lines_list:
                if line.get_points()[1] is not None:

                    inter_point = Intercection_funct.check_lines_intercection(line, edge)
                    if inter_point is not None:
                        intercection_points_list.append(inter_point)

    # Держим цикл на правильной скорости
    clock.tick(FPS)
    # Ввод процесса (события)
    for event in pygame.event.get():
        
        # check for closing window
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_p:
                work_with_polygon = True
                work_with_line = False

            if event.key == pygame.K_l:
                work_with_line = True
                work_with_polygon = False

            if event.key == pygame.K_d:
                polygons_list, lines_list = download_picture()

                work_with_line = False
                work_with_polygon = False
                finish_polygon = True
                finish_line = True

            if event.key == pygame.K_s:
                save_picture(polygons_list, lines_list)

        if event.type == pygame.MOUSEBUTTONDOWN:
            new_vertex = Point(event.pos[0], event.pos[1])
            if event.button == 1 and work_with_polygon:
                if finish_polygon or len(polygons_list) == 0:
                    polygons_list.append(Polygon(new_vertex))
                    finish_polygon = False
                else:
                    polygons_list[-1].append_vertex(new_vertex)

            if event.button == 1 and work_with_line:
                if finish_line or len(lines_list) == 0:
                    lines_list.append(Line(new_vertex))
                    finish_line = False
                else:
                    lines_list[-1].record_finish_point(new_vertex)

            if event.button == 3:
                work_with_line = False
                work_with_polygon = False
                finish_polygon = True
                finish_line = True


    # Обновление
    
    # Рендеринг
    screen.fill(WHITE)
    for point in intercection_points_list:
        pygame.draw.circle(screen, RED, (point.x, point.y), 5)

    if len(polygons_list) != 0:
        for polygon in polygons_list:
            polygon.draw(screen)

            for vertex in polygon.vertex_list:    
                draw_circle_in_vertex(vertex)

    if len(lines_list) != 0:
        for line in lines_list:
            line.draw(screen)

            line_pos = line.get_points()
            draw_circle_in_vertex(line_pos[0])
            if line_pos[1] is not None:
                draw_circle_in_vertex(line_pos[1])

    # После отрисовки всего, переворачиваем экран
    pygame.display.flip()

pygame.quit()