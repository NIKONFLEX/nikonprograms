import pygame
from Point_class import Point

class Line:
    def __init__(self, start_point:Point):
        self.__start_point = start_point
        self.__finish_point = None

    def record_finish_point(self, finish_point:Point):
        self.__finish_point = finish_point

    def get_points(self):
        return [self.__start_point, self.__finish_point]

    def draw(self, screen):
        if self.__finish_point is not None:
            pygame.draw.line(screen, (0, 255, 0), [self.__start_point.x, self.__start_point.y], [self.__finish_point.x, self.__finish_point.y], 2)


    