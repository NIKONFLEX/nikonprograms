class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def get_in_list(self):
        return [self.x, self.y]
