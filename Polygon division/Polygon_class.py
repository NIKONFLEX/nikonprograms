import pygame
from Point_class import Point

class Polygon:
    def __init__(self, first_vertex : Point):
        self.vertex_list = [first_vertex]

    def append_vertex(self, vertex : Point):
        self.vertex_list.append(vertex)

    def draw(self, screen):
        if len(self.vertex_list) >= 2:
            vertex_list_for_draw = self.points_list_to_pairs_list()
            pygame.draw.polygon(screen, (0, 0, 0), vertex_list_for_draw, 2)

    def points_list_to_pairs_list(self):
        new_list = []
        for point in self.vertex_list:
            new_list.append(point.get_in_list())

        return new_list
        
