from Point_class import Point
from Line_class import Line

def check_lines_intercection(line_1:Line, line_2:Line):
    line_1_param = line_1.get_points()
    x1_1, y1_1 = line_1_param[0].x, line_1_param[0].y
    x1_2, y1_2 = line_1_param[1].x, line_1_param[1].y
    line_2_param = line_2.get_points()
    x2_1, y2_1 = line_2_param[0].x, line_2_param[0].y
    x2_2, y2_2 = line_2_param[1].x, line_2_param[1].y
    
    A1 = y1_1 - y1_2
    B1 = x1_2 - x1_1
    C1 = x1_1 * y1_2 - x1_2 * y1_1
    A2 = y2_1 - y2_2
    B2 = x2_2 - x2_1
    C2 = x2_1 * y2_2 - x2_2 * y2_1
    
    if B1 * A2 - B2 * A1 != 0:
        y = (C2 * A1 - C1 * A2) / (B1 * A2 - B2 * A1)
        x = (-C1 - B1 * y) / A1
    
        if min(x1_1, x1_2) <= x <= max(x1_1, x1_2) and min(y1_1, y1_2) <= y <= max(y1_1, y1_2) \
        and min(x2_1, x2_2) <= x <= max(x2_1, x2_2) and min(y2_1, y2_2) <= y <= max(y2_1, y2_2):
            return Point(int(x), int(y))

        else:
            return None
    
    if B1 * A2 - B2 * A1 == 0:
        return None