import json

def read_file_with_polygon_and_line():
    try:
        with open('Saved_polygon.json') as polygon_and_line:
            polygon_and_line = json.load(polygon_and_line)
        return Saved_image(polygon_and_line['polygon'], polygon_and_line['line'])
    except:
        return Saved_image(None, None)
    
class Saved_image:
    def __init__(self, polygon, line):
        self.polygon = polygon
        self.line = line

    def write_polygon_and_line(self):
        with open('Saved_polygon.json', 'w') as save_polygon:
            json.dump(vars(self), save_polygon)