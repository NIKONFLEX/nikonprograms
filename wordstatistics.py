import string
import collections

def _read_file(name_of_file):
    read_list = open(name_of_file, 'r', encoding = "utf-8")
    readed_file = read_list.read()
    read_list.close()
    return readed_file

def _split_file_to_words(unsplit_file):
    split_file = unsplit_file.split()
    return split_file

def _delete_digits_and_numbers(unsplitted_file):
    punctuations = string.punctuation
    numbers = '0123456789-'
    for i in punctuations:
        unsplitted_file = unsplitted_file.replace(i, '')
    for i in numbers:
        unsplitted_file = unsplitted_file.replace(i, '')

    return unsplitted_file

def _delete_prepositions(txt_list, list_of_prepositions):
    without_prepositions = []
    for word in txt_list:
        if not word.lower() in list_of_prepositions:
            without_prepositions.append(word)
    return without_prepositions

def _word_counter(txt_list_without_prepositions, num_of_couples):
    dict_of_words = collections.Counter()
    for word in txt_list_without_prepositions:
        if len(word) > 2:
            if word.lower() in dict_of_words:
                dict_of_words[word.lower()] += 1
            else:
                dict_of_words[word.lower()] = 1 
    five_most_frequently_words = dict_of_words.most_common(num_of_couples)
    return five_most_frequently_words

def get_word_statics(file_txt_name, n_of_couples, exclude_word_list):
    txt_file = _read_file(file_txt_name)
    prepositions =  _read_file('prepositions.txt')      #считаны файлы
    
    non_digits_txt_file = _delete_digits_and_numbers(txt_file)
    txt_file_list = _split_file_to_words(non_digits_txt_file)    #список всех слов
    
    prepositions_list = _split_file_to_words(prepositions)      #список предлогов
    
    txt_file_without_prepositions = _delete_prepositions(txt_file_list, prepositions_list + exclude_word_list)
    most_frequently = _word_counter(txt_file_without_prepositions, n_of_couples)     #выдает пары слов и чисел

    return most_frequently

