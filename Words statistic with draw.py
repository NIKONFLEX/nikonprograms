import pygame
import random
import wordstatistics

WIDTH = 600
HEIGHT = 600
FPS = 30

# Задаем цвета
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)

exclude_word_list = []

def draw_column(measure_of_column, pair_of_word_and_n, repetition_place):
    height_of_column = measure_of_column * int(pair_of_word_and_n[1])
    width_of_column = 70
    x_of_column = 100 + 100 * repetition_place
    pygame.draw.rect(screen, (10, 0, 65), (100 + 100 * repetition_place, HEIGHT, width_of_column, -height_of_column))      #рисуются колоннs

    font_size = 20
    word = pair_of_word_and_n[0]
    font = pygame.font.SysFont("Agency FB", font_size)    
    label1 = font.render(word, 1, BLACK)
    label1.set_alpha(100)
    textRectObj = label1.get_rect()
    textRectObj.center = (x_of_column + width_of_column // 2, HEIGHT - height_of_column)

    width_of_word = textRectObj.size[0]
    center_of_word = textRectObj.center[1]
    min_offset_from_column = 1                      #расстояние между колонной и низом слова
    offset_from_column = HEIGHT - height_of_column - textRectObj.midbottom[1]

    if width_of_word > width_of_column:
        while width_of_word > width_of_column:
            font_size -= 1
            font = pygame.font.SysFont("Agency FB", font_size)    
            label1 = font.render(word, 1, BLACK)
            label1.set_alpha(100)
            textRectObj = label1.get_rect()
            width_of_word = textRectObj.size[0]
            textRectObj.center = (x_of_column + width_of_column // 2, center_of_word)
            width_of_word = textRectObj.size[0]
            center_of_word = textRectObj.center[1]
            offset_from_column = HEIGHT - height_of_column - textRectObj.midbottom[1]

    if offset_from_column < min_offset_from_column:
        while offset_from_column < min_offset_from_column:
            center_of_word = textRectObj.center[1] - 1
            font = pygame.font.SysFont("Agency FB", font_size)    
            label1 = font.render(word, 1, BLACK)
            label1.set_alpha(100)
            textRectObj = label1.get_rect()
            textRectObj.center = (x_of_column + width_of_column // 2, center_of_word)
            offset_from_column = HEIGHT - height_of_column - textRectObj.midbottom[1]

    screen.blit(label1, textRectObj) 
    txt_rect = textRectObj
    return txt_rect

def draw_txt_with_file_name(file_name):
    font_size = 20
    phrase = 'words statistic from file ' + "'" + file_name + "'"
    font = pygame.font.SysFont("Agency FB", font_size)    
    label1 = font.render(phrase, 1, BLACK)
    label1.set_alpha(100)
    textRectObj = label1.get_rect()
    textRectObj.center = (WIDTH / 2, 15)
    screen.blit(label1, textRectObj) 

def count_measure(max_n_of_rep):
    measure = (HEIGHT - 100) / max_n_of_rep            #размер одного деления
    return measure

def draw_marker(measure, max_n_of_rep):
    height_of_marker = measure * max_n_of_rep
    division_size_in_pix = height_of_marker / 10
    division_size_in_rep = int(division_size_in_pix // measure)
    n_divisions = 10
    pygame.draw.line(screen, BLACK, [20, HEIGHT], [20,  HEIGHT - height_of_marker], 1)  

    for i in range(n_divisions):
        pygame.draw.line(screen, BLACK, [20, HEIGHT - division_size_in_pix - division_size_in_pix * i], [40, HEIGHT - division_size_in_pix - division_size_in_pix * i], 1)    
        str_point_of_division = str((division_size_in_rep + division_size_in_rep * i) - (division_size_in_rep + division_size_in_rep * i) % 10) 
        font = pygame.font.SysFont("Agency FB", 20)    
        label1 = font.render(str_point_of_division, 1, BLACK)
        label1.set_alpha(100)
        textRectObj = label1.get_rect()
        textRectObj.center = (60, HEIGHT - division_size_in_pix - division_size_in_pix * i)
        screen.blit(label1, textRectObj)   

def draw_marker_for_five_repeats(measure):
    n_of_divisions = 5
    pygame.draw.line(screen, BLACK, [20, HEIGHT], [20, HEIGHT - n_of_divisions * measure], 1)  
    for i in range(n_of_divisions):
        pygame.draw.line(screen, BLACK, [20, HEIGHT - measure - measure * i], [40, HEIGHT - measure - measure * i], 1)    
        str_point_of_division = str(1 + 1 * i) 
        font = pygame.font.SysFont("Agency FB", 20)    
        label1 = font.render(str_point_of_division, 1, BLACK)
        label1.set_alpha(100)
        textRectObj = label1.get_rect()
        textRectObj.center = (60, HEIGHT - measure - measure * i)
        screen.blit(label1, textRectObj)   

def check_cross_mouse_and_words(pos_mouse, obj):
    cross = False
    obj_x = obj.bottomleft[0]
    obj_y = obj.bottomleft[1]
    obj_width = obj.size[0]
    obj_height = obj.size[1]

    obj_up = obj_y - obj_height
    obj_down = obj_y
    obj_right = obj_x + obj_width
    obj_left = obj_x

    if obj_up > pos_mouse[1] or obj_down < pos_mouse[1]:
        cross = False
    else:
        cross = True

    if obj_left > pos_mouse[0] or obj_right < pos_mouse[0]:
        cross = False
    else:
        cross = True
    return cross

def return_word(pos_of_word, list_of_words):
    word = list_of_words[pos_of_word][0]
    return word

def append_word_to_delete_list(word):
    exclude_word_list.append(word)

#получение статистики чисел
name_of_txt_file = 'python book.txt'

word_statistics = wordstatistics.get_word_statics(name_of_txt_file, 5, exclude_word_list)
print(word_statistics)

txt_rects_list = []
txt_rects_list_print = False
delete_word = False

# Создаем игру и окно
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("My Game")
clock = pygame.time.Clock()

# Цикл игры
running = True
while running:
    if delete_word:
        append_word_to_delete_list(returned_word)
        word_statistics = wordstatistics.get_word_statics(name_of_txt_file, 5, exclude_word_list)
        print(word_statistics)
        delete_word = False

    clock.tick(FPS)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
            mouse_pos = event.pos
            for obj in txt_rects_list:
                cross = check_cross_mouse_and_words(mouse_pos, obj)
                if not cross:
                    print('NOT CROSS')
                else:
                    n_of_cross_word = txt_rects_list.index(obj)
                    returned_word = return_word(n_of_cross_word, word_statistics)
                    print('CROSS', n_of_cross_word, returned_word)
                    delete_word = True
    
    # Рендеринг
    screen.fill(WHITE)
    the_measure = count_measure(word_statistics[0][1])
    if word_statistics[0][1] <= 5:
        draw_marker_for_five_repeats(the_measure)
    else:
        draw_marker(the_measure, word_statistics[0][1])
    txt_rects_list = []
    for i in range(len(word_statistics)):
        txt_rect = draw_column(the_measure, word_statistics[i], i)
        txt_rects_list.append(txt_rect)
    draw_txt_with_file_name(name_of_txt_file)
    pygame.display.flip()
    if not txt_rects_list_print:
        print(txt_rects_list)
        txt_rects_list_print = True

pygame.quit()