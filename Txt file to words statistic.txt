
We use cookies to personalize content and ads, to provide social media features and to analyze our traffic. We also share information about your use of our site with our social media, advertising and analytics partners. Privacy Policy

Accept Cookies
Cookie Settings
SKIP TO MAIN CONTENT

Subscribe
Latest Issues
Scientific American
Cart 0Sign In|Stay Informed
SHARELATEST
ObservationsObservations | Opinion
Comparing COVID-19 Deaths to Flu Deaths Is like Comparing Apples to Oranges
The former are actual numbers; the latter are inflated statistical estimates

By Jeremy Samuel Faust on April 28, 2020
Comparing COVID-19 Deaths to Flu Deaths Is like Comparing Apples to Oranges
Credit: Philippe Lopez Getty Images
In late February, when the stock market was beginning to fall over coronavirus fears, President Donald Trump held a briefing at the White House to reassure people that there was little chance of the virus causing significant disruption in the United States.

“I want you to understand something that shocked me when I saw it,” he said. “The flu, in our country, kills from 25,000 people to 69,000 people a year. That was shocking to me.”

His point was to suggest that the coronavirus was no worse than the flu, whose toll of deaths most of us apparently barely noticed.


ADVERTISEMENT
In early April, as social distancing measures began to succeed in flattening the curve in some parts of the country, an influential forecasting model revised the number of American deaths from coronavirus that it was projecting by summer downward to 60,400, and some people again began making comparisons to the flu, arguing that, if this will ultimately be no worse than a bad flu season, we should open the country up for business again. (On April 22, the model’s forecast rose to 67,641 deaths.)

But these arguments, like the president’s comments, are based on a flawed understanding of how flu deaths are counted, which may leave us with a distorted view of how coronavirus compares with it.

When reports about the novel coronavirus SARS-CoV-2 began circulating earlier this year and questions were being raised about how the illness it causes, COVID-19, compared to the flu, it occurred to me that, in four years of emergency medicine residency and over three and a half years as an attending physician, I had almost never seen anyone die of the flu. I could only remember one tragic pediatric case.

Based on the CDC numbers though, I should have seen many, many more. In 2018, over 46,000 Americans died from opioid overdoses. Over 36,500 died in traffic accidents. Nearly 40,000 died from gun violence. I see those deaths all the time. Was I alone in noticing this discrepancy?

I decided to call colleagues around the country who work in other emergency departments and in intensive care units to ask a simple question: how many patients could they remember dying from the flu? Most of the physicians I surveyed couldn’t remember a single one over their careers. Some said they recalled a few. All of them seemed to be having the same light bulb moment I had already experienced: For too long, we have blindly accepted a statistic that does not match our clinical experience.


ADVERTISEMENT
The 25,000 to 69,000 numbers that Trump cited do not represent counted flu deaths per year; they are estimates that the CDC produces by multiplying the number of flu death counts reported by various coefficients produced through complicated algorithms. These coefficients are based on assumptions of how many cases, hospitalizations, and deaths they believe went unreported. In the last six flu seasons, the CDC’s reported number of actual confirmed flu deaths—that is, counting flu deaths the way we are currently counting deaths from the coronavirus—has ranged from 3,448 to 15,620, which far lower than the numbers commonly repeated by public officials and even public health experts.

There is some logic behind the CDC’s methods. There are, of course, some flu deaths that are missed, because not everyone who contracts the flu gets a flu test. But there are little data to support the CDC’s assumption that the number of people who die of flu each year is on average six times greater than the number of flu deaths that are actually confirmed. In fact, in the fine print, the CDC’s flu numbers also include pneumonia deaths.

The CDC should immediately change how it reports flu deaths. While in the past it was justifiable to err on the side of substantially overestimating flu deaths, in order to encourage vaccination and good hygiene, at this point the CDC’s reporting about flu deaths is dangerously misleading the public and even public officials about the comparison between these two viruses. If we incorrectly conclude that COVID-19 is “just another flu,” we may retreat from strategies that appear to be working in minimizing the speed of spread of the virus.

newsletter promo
Sign up for Scientific American’s free newsletters.

Sign Up
The question remains. Can we accurately compare the toll of the flu to the toll of the coronavirus pandemic?

To do this, we have to compare counted deaths to counted deaths, not counted deaths to wildly inflated statistical estimates. If we compare, for instance, the number of people who died in the United States from COVID-19 in the second full week of April to the number of people who died from influenza during the worst week of the past seven flu seasons (as reported to the CDC), we find that the novel coronavirus killed between 9.5 and 44 times more people than seasonal flu. In other words, the coronavirus is not anything like the flu: It is much, much worse.

ADVERTISEMENT
From this perspective, the data on coronavirus and flu actually match—rather than flying in the face of—our lived reality in the coronavirus pandemic: hospitals in hot spots stretched to their limits and, in New York City in particular, so many dead that the bodies are stacked in refrigerator trucks. We have never seen such conditions.

In that briefing in late February, Trump downplayed the likelihood that the virus would spread significantly in the United States and that extreme measures like closing schools would need to be taken, saying that “we have it so well under control” and returning again to the flu.

“Sixty-nine thousand people die every year—from 26 to 69—every year from the flu,” he said. “Now, think of that. It’s incredible.”

We now know that Trump was disastrously wrong about the threat that the coronavirus posed to the United States. But his take that the cited numbers of flu deaths were incredible? On that, he was spot-on.

The opinions expressed in this article are solely those of the author and do not reflect the views and opinions of Brigham and Women’s Hospital or Harvard Medical School.

ADVERTISEMENT
The views expressed are those of the author(s) and are not necessarily those of Scientific American.

Rights & Permissions
ABOUT THE AUTHOR(S)
Jeremy Samuel Faust
Jeremy Samuel Faust, M.D., M.S., M.A., FACEP, practices emergency medicine at Brigham & Women's Hospital, is an instructor at Harvard Medical School, and is president of the Roomful of Teeth Vocal Arts Project.

READ THIS NEXT
BEHAVIOR & SOCIETY
How Behavioral Science Can Help Contain the Coronavirus
May 11, 2020 — Jocelyn Bélanger | Opinion

ENGINEERING
Torpedoes Are Greatly Overrated as Naval Weapon
2 hours ago

BEHAVIOR & SOCIETY
Addressing the Stigma That Surrounds Addiction
5 hours ago — Nora D. Volkow | Opinion

ENGINEERING
Pumping Charged Particles onto Airplane Surfaces Could Reduce Lightning Strikes  
5 hours ago — Mark Fischetti

SPACE
How to Grow Vegetables on Mars
May 10, 2020 — Edward Guinan, Scott Engle and Alicia Eglin | Opinion

POLICY & ETHICS
In Case You Missed It
May 10, 2020 — Sarah Lewin Frasier


ADVERTISEMENT
NEWSLETTER
Get smart. Sign up for our email newsletter.
Sign Up
READ MORE
PREVIOUS
Self-Isolation? I'm an Expert
By Melissa T. Miller on April 27, 2020
NEXT
Will Americans Be Willing to Install COVID-19 Tracking Apps?
By Eszter Hargittai and Elissa Redmiles on April 28, 2020
Support Science Journalism
Discover world-changing science. Explore our digital archive back to 1845, including articles by more than 150 Nobel Prize winners.

Subscribe Now!Support Science Journalism
FOLLOW US

instagram youtube twitter facebook rss
SCIENTIFIC AMERICAN ARABIC

العربية
Return & Refund Policy
About
Press Room
 
FAQs
Contact Us
Site Map
 
Advertise
SA Custom Media
Terms of Use
 
Privacy Policy
Use of Cookies
International Editions
Scientific American is part of Springer Nature, which owns or has commercial relations with thousands of scientific publications (many of them can be found at www.springernature.com/us). Scientific American maintains a strict policy of editorial independence in reporting developments in science to our readers.
© 2020 SCIENTIFIC AMERICAN, A DIVISION OF NATURE AMERICA, INC.

ALL RIGHTS RESERVED.

SCROLL TO TOP